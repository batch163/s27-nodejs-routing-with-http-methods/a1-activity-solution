const HTTP = require('http');
const PORT = 3003;

HTTP.createServer((req, res) => {
// Create a simple server and the following routes with their corresponding HTTP methods and responses:

// If the url is http://localhost:4000/, send a response Welcome to Booking System
if(req.url == "/" && req.method == "GET"){
    res.writeHead(200, {"Content-Type": "text/plain"})
    res.write("Welcome to Booking System!")
    res.end()
}
// If the url is http://localhost:4000/profile, send a response Welcome to your profile!
if(req.url == "/profle" && req.method == "GET"){
    res.writeHead(200, {"Content-Type": "text/plain"})
    res.write("Welcome to your profile!")
    res.end()
}
// If the url is http://localhost:4000/courses, send a response Here’s our courses available
if(req.url == "/courses" && req.method == "GET"){
    res.writeHead(200, {"Content-Type": "text/plain"})
    res.write("Here’s our courses available")
    res.end()
}
// If the url is http://localhost:4000/addcourse, send a response Add a course to our resources
if(req.url == "/addcourse" && req.method == "POST"){
    res.writeHead(200, {"Content-Type": "text/plain"})
    res.write("Add a course to our resources")
    res.end()
}


// Create a simple server and the following routes with their corresponding HTTP methods and responses:

// If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
if(req.url == "/updatecourse" && req.method == "PUT"){
    res.writeHead(200, {"Content-Type": "text/plain"})
    res.write("Update a course to our resources")
    res.end()
}

// If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources
if(req.url == "/archivecourses" && req.method == "DELETE"){
    res.writeHead(200, {"Content-Type": "text/plain"})
    res.write("Archive courses to our resources")
    res.end()
}


}).listen(PORT, () => console.log(`Server connected to port ${PORT}`))



